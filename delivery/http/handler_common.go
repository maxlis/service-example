package http

import (
	"net/http"
)

func (s *service) HealthHandler(w http.ResponseWriter, r *http.Request) {
	if err := s.u.Health(r.Context()); err != nil {
		// log + err
		return
	}

	// return heath check
}
