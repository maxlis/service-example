package http

import (
	"net/http"

	"github.com/gorilla/mux"
)

// ParcelHandler is our /v1/parcel
//
// GET				/v1/parcel/list 				            	| get all parcels
type parcel struct {
	factory
}

func (p *parcel) route(r *mux.Router) {
	r.Path("/list").Methods(http.MethodGet).HandlerFunc(p.List)
	r.Path("/list/{id}").Methods(http.MethodGet).HandlerFunc(p.List)
	r.Path("/list").Methods(http.MethodGet).HandlerFunc(p.List)
	r.Path("/list").Methods(http.MethodGet).HandlerFunc(p.List)
	r.Path("/list").Methods(http.MethodGet).HandlerFunc(p.List)
}

// Get retrieve parcel list
// swagger:route GET /v1/parcel/list Parcel parcelList
//
// List
//
//     Responses:
//       200: ParcelListResponse OK
//       default: Response Error
func (p *parcel) List(w http.ResponseWriter, r *http.Request) {

	//	p.u.GetParcel(ctx, req.id)
}
