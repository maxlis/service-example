package http

import (
	"fmt"

	"github.com/gorilla/mux"
	"gitlab.com/maxlis/service-example/usecase"
)

type factory struct {
	u usecase.Interface
}

type Opt string

const (
	Pa Opt = "parcel"
)

func (f factory) FactoryRoute(o Opt, r *mux.Router) {
	switch o {
	case Pa:
		g := &parcel{factory: f}
		g.route(r)
	default:
		fmt.Println("test")
	}
}
