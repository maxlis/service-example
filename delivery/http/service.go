package http

import (
	"context"
	"fmt"
	"net"
	"net/http"
	"time"

	"github.com/gorilla/mux"

	"gitlab.com/maxlis/service-example/delivery"
	"gitlab.com/maxlis/service-example/usecase"
)

type service struct {
	factory
	server *http.Server
}

func New(u usecase.Interface) delivery.Interface {
	s := &http.Server{}

	return &service{server: s, factory: factory{u: u}}
}

func (s *service) route() http.Handler {
	r := mux.NewRouter()
	r.HandleFunc("/health", s.HealthHandler).Methods(http.MethodGet)

	// middleware use with r.use

	s.FactoryRoute(Pa, r.PathPrefix("/parcel").Subrouter())

	return r
}

func (s *service) Serve(listener net.Listener) {
	s.server.Handler = s.route()

	if err := s.server.Serve(listener); err != nil {
		// log
		fmt.Println(err)
	}
}

func (s *service) Stop() {
	ctx, closer := context.WithTimeout(context.Background(), time.Minute)
	defer closer()

	if err := s.server.Shutdown(ctx); err != nil {
		// log
		fmt.Println(err)
	}
}
