package rpc

import (
	"google.golang.org/grpc"
	"google.golang.org/grpc/health/grpc_health_v1"

	"gitlab.com/maxlis/service-example/usecase"
)

type factory struct {
	u usecase.Interface
}

type Opt string

const (
	He Opt = "health"
	Pa Opt = "parcel"
)

func (f factory) FactoryRoute(o Opt, r *grpc.Server) {
	switch o {
	case Pa:

	case He:
		grpc_health_v1.RegisterHealthServer(r, &HealthCheck{factory: f})
	}
}
