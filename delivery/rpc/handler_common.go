package rpc

import (
	"context"

	"google.golang.org/grpc/health/grpc_health_v1"
)

type HealthCheck struct {
	factory
}

func (h *HealthCheck) Check(ctx context.Context, _ *grpc_health_v1.HealthCheckRequest) (*grpc_health_v1.HealthCheckResponse, error) {
	status := grpc_health_v1.HealthCheckResponse_SERVING
	if err := h.u.Health(ctx); err != nil {
		status = grpc_health_v1.HealthCheckResponse_NOT_SERVING
	}

	return &grpc_health_v1.HealthCheckResponse{Status: status}, nil
}

func (h *HealthCheck) Watch(request *grpc_health_v1.HealthCheckRequest, server grpc_health_v1.Health_WatchServer) error {
	panic("implement me")
}
