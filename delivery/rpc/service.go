package rpc

import (
	"fmt"
	"net"

	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	grpc_recovery "github.com/grpc-ecosystem/go-grpc-middleware/recovery"
	"google.golang.org/grpc"

	"gitlab.com/maxlis/service-example/delivery"
	"gitlab.com/maxlis/service-example/usecase"
)

type service struct {
	factory
	server *grpc.Server
}

func New(u usecase.Interface) delivery.Interface {
	grpc.EnableTracing = true
	// Shared options for the logger, with a custom gRPC code to log level function.
	opts := []grpc_recovery.Option{
		grpc_recovery.WithRecoveryHandlerContext(nil),
	}
	srv := grpc.NewServer(
		grpc_middleware.WithUnaryServerChain(
			grpc_recovery.UnaryServerInterceptor(opts...),
		),

		grpc_middleware.WithStreamServerChain(
			grpc_recovery.StreamServerInterceptor(opts...),
		),
	)

	return &service{factory: factory{u: u}, server: srv}
}

func (g *service) route() {
	g.FactoryRoute(Pa, g.server)
	g.FactoryRoute(He, g.server)
}

func (g *service) Serve(listener net.Listener) {
	g.route()

	if err := g.server.Serve(listener); err != nil {
		// log
		fmt.Println(err)
	}
}

func (g *service) Stop() {
	if g.server == nil {
		panic("server not exist")
	}

	g.server.GracefulStop()
}
