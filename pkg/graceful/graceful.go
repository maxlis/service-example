package graceful

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"
)

const (
	defaultWaitSec = 2
)

type Queue struct {
	fifo []func()
}

// New instance of graceful shutdfown.
func New() *Queue {
	return &Queue{fifo: make([]func(), 0, 1)}
}

// Add to graceful shutdown queue task
func (q *Queue) Add(fn func()) {
	q.fifo = append(q.fifo, fn)
}

// Shutdown lock main thread and wait signal for stop app.
// also perform some queued tasks before stop in specific order.
func (q *Queue) Shutdown() {
	var gracefulStop = make(chan os.Signal, 1)

	signal.Notify(gracefulStop, syscall.SIGTERM)
	signal.Notify(gracefulStop, syscall.SIGINT)
	signal.Notify(gracefulStop, syscall.SIGILL)

	sig := <-gracefulStop
	fmt.Println("graceful shutdown", sig.String(), len(q.fifo))

	for i, cb := range q.fifo {
		fmt.Println("graceful start task", i+1)
		cb()
		fmt.Println("graceful complete task", i+1)
		<-time.After(defaultWaitSec * time.Second)
	}

	time.Sleep(defaultWaitSec * time.Second)
	fmt.Println("graceful shutdown complete")
	os.Exit(0)
}
