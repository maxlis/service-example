package config

type Config struct {
	HTTPPort int
	GRpcPort int
}

var (
	Cfg *Config
)

func Initialize() {
	Cfg = &Config{
		HTTPPort: 8080,
		GRpcPort: 9090,
	}
}
