module gitlab.com/maxlis/service-example

go 1.15

require (
	github.com/gorilla/mux v1.8.0
	github.com/grpc-ecosystem/go-grpc-middleware v1.2.2
	google.golang.org/grpc v1.36.0
)
