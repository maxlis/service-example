package controller

import (
	"context"
	"time"

	"gitlab.com/maxlis/service-example/repository"
	"gitlab.com/maxlis/service-example/usecase"
)

type service struct {
	repo    repository.Interface
	timeout time.Duration
}

func New(r repository.Interface, timeout time.Duration) usecase.Interface {
	return &service{repo: r, timeout: timeout}
}

func (s *service) Health(_ctx context.Context) error {
	_, cancel := context.WithTimeout(_ctx, s.timeout)
	defer cancel()

	// return s.repo.Health(ctx)
	return nil
}
