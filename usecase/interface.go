package usecase

import (
	"context"
)

type Interface interface {
	Health(_ctx context.Context) error
}
