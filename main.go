package main

import (
	"fmt"
	"net"
	"time"

	"gitlab.com/maxlis/service-example/config"
	"gitlab.com/maxlis/service-example/delivery"
	"gitlab.com/maxlis/service-example/delivery/http"
	"gitlab.com/maxlis/service-example/pkg/graceful"
	"gitlab.com/maxlis/service-example/usecase"
	"gitlab.com/maxlis/service-example/usecase/controller"
)

func main() {
	config.Initialize()

	// cassandra := cassandra.New() init casandra repo
	// elastic := elastic.New() init elastic repo
	uc := controller.New(nil, time.Second)

	rest := httpInit(uc)

	grace := graceful.New()
	grace.Add(rest.Stop)

	grace.Shutdown()
}

func httpInit(uc usecase.Interface) delivery.Interface {
	// ep := fmt.Sprintf("0.0.0.0:%s", config.Cfg.HttpPort)
	ep := "0.0.0.0:1111"
	res := http.New(uc)
	listen, err := net.Listen("tcp", ep) //nolint:gosec
	if err != nil {
		// log
		fmt.Println(err)
	}

	go res.Serve(listen)

	// log about start

	return res
}
